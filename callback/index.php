<?php
session_start(); // en PHP très important sinon aucune SESSION n'est sauvegarder en dehors de ce script
require __DIR__.'/./vendor/autoload.php';

require('src/OAuth2/Client.php');
require('src/OAuth2/GrantType/IGrantType.php');
require('src/OAuth2/GrantType/AuthorizationCode.php');

// Identifiant et Mot de passe sont à demander au SiMDE en précisant qui est touché par votre
// application ainsi que le lien de redirection (ici http://assos.utc.fr/votre_asso/callback)
const CLIENT_ID     = 'id_du_client';
const CLIENT_SECRET = 'motdepasse_du_client';

const REDIRECT_URI           = 'http://assos.utc.fr/votre_asso/callback'; // lien de traitement du retour
const AUTHORIZATION_ENDPOINT = 'https://assos.utc.fr/oauth/authorize'; // lien d'autorisation
const TOKEN_ENDPOINT         = 'https://assos.utc.fr/oauth/token'; // lien de récupération du token

$client = new OAuth2\Client(CLIENT_ID, CLIENT_SECRET);
if (!isset($_GET['code']))
{
    $auth_url = $client->getAuthenticationUrl(AUTHORIZATION_ENDPOINT, REDIRECT_URI, array('scope' => 'user-get-info-identity'));
    // Toutes les catégories des 'scopes' sont disponibles à l'adresse https://assos.utc.fr/oauth/scopes/categories (à ouvrir sous Firefox pour mettre en catégorie)
    header('Location: ' . $auth_url);
    exit;
}
else
{
    $params = array('code' => $_GET['code'], 'redirect_uri' => REDIRECT_URI);
    $response = $client->getAccessToken(TOKEN_ENDPOINT, 'authorization_code', $params);
    
    $info = $response['result'];

    $client->setAccessToken($info['access_token']);
    $response = $client->fetch(
            'https://assos.utc.fr/api/v1/user',
            array(),
            'GET',
            array('Authorization' => 'Bearer '. $info['access_token']) // très important sinon pas d'autorisation et pas d'autorisation pas de connexion et pas de connexion pas de données et pas de données... bah pas de données.
        );
    $userdata = $response['result'];

    // récupération des informations que vous avez demandés au Portail
    $_SESSION['user'] = $userdata['id'];
    $_SESSION['nom'] = $userdata['lastname'];
    $_SESSION['prenom'] = $userdata['firstname'];
    $_SESSION['mail'] = $userdata['email'];

    header('Location: ../');
}